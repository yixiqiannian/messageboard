# messageboard

#### 介绍
vue+python+mysql+flask+element+axios实现留言板功能,前后端分离小项目，演示地址[http://am.qqvxzf.cn/](http://am.qqvxzf.cn/)（被攻击，已关闭）

#### 项目演示
![演示](https://foruda.gitee.com/images/1716432911585564084/b0c257de_8386676.png "屏幕截图")


#### 后端python使用教程

0.  确保python环境为3.11，其他版本自测，保险起见最好使用3.11版本。
1.  `pip install -r requirements.txt`//此方法如果不行，则用下面的教程
2.  `pip3 install flask`
3.  `pip3 install flask-cors`
4.  `pip3 install pymysql`
5.  `python3 messageboardapi.py`//运行项目

#### 前端

前端部分没什么好说的，直接运行就行了



#### 说明
一个简单的练手项目而已，觉得对你有帮助的话，劳烦star一下。
如果能赏点饭钱，那就更好了
![输入图片说明](https://foruda.gitee.com/images/1716433735916793430/304bd643_8386676.jpeg "zfb.jpg")
![输入图片说明](https://foruda.gitee.com/images/1716433742288474136/a1cd6642_8386676.jpeg "vx.jpg")

